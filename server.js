var express = require('express');
var passport = require('passport');
var Strategy = require('passport-local').Strategy;
var db = require('./db');
var Auth = require('./db/auth.js');
var DbAuth = require('./db/dbAuth.js');
var DbQuerys = require('./db/dbQuerys.js');
const https = require('https');
const fs = require('fs');
const WebSocket = require('ws');
const Db = require('./db/db.js');
const auth = new Auth();
const DB = new Db();
const dbAuth = new DbAuth();
const dbQuerys = new DbQuerys(DB,auth);
const privateKey  = fs.readFileSync('./certificates/selfsigned.key', 'utf8');
const certificate = fs.readFileSync('./certificates/selfsigned.crt', 'utf8');

// Configure the local strategy for use by Passport.
//
// The local strategy require a `verify` function which receives the credentials
// (`username` and `password`) submitted by the user.  The function must verify
// that the password is correct and then invoke `cb` with a user object, which
// will be set at `req.user` in route handlers after authentication.
passport.use(new Strategy(
  function(username, password, cb) {
    db.users.findByUsernameInDB(username,password, function(err, signedin) {
      if (err) { return cb(err); }
      if (!signedin) { return cb(null, false); }
      return cb(null, signedin);
    });
  }));

// Configure Passport authenticated session persistence.
//
// In order to restore authentication state across HTTP requests, Passport needs
// to serialize users into and deserialize users out of the session.  The
// typical implementation of this is as simple as supplying the user ID when
// serializing, and querying the user record by ID from the database when
// deserializing.

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  db.users.findById(id, function (err, user) {
    if (err) {
        return cb(err,null);
    }
    cb(null, user);
  });
});



// Create a new Express application.
var app = express();

// Configure view engine to render EJS templates.
app.set('views', __dirname + '/public/views');
app.set('view engine', 'ejs');

// Use application-level middleware for common functionality, including
// logging, parsing, and session handling.
app.use(express.static(__dirname + '/public'));
app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(require('express-session')({ secret: 'keyboard cat', resave: false, saveUninitialized: false }));

// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize());
app.use(passport.session());

function isAuthenticated(req, res, next) {
    // do any checks you want to in here
    if(typeof req.user !== 'undefined'){
        db.users.findById(req.user.id,function(err, user){
            if (!err && req.user == user) {
                console.log("hei");
                return next();
            }else{
                // IF A USER ISN'T LOGGED IN, THEN REDIRECT THEM SOMEWHERE
                res.redirect('/login');
            }
        });
    }else{
        res.redirect('/login');
    }
    //console.log(req.user);
}

// Define routes.
app.get('/login',
    function(req, res){
        res.render('login');
    });

app.post('/login',
    passport.authenticate('local', { failureRedirect: '/login' }),
    function(req, res) {
        res.redirect('/home');
    });

app.post('/newUser',isAuthenticated,function(req,res){
    console.log("New user request!");
    console.log(req.body);
    dbQuerys.addNewUser(req.body.name,req.body.username,req.body.password,"1",function (result) {
        res.redirect('/home');
    });
});

app.post('/deleteUser',isAuthenticated,function(req,res){
    console.log("New user delete request!");
    console.log(req.body);
    dbQuerys.removeUser(req.body.id,function(result){
        if(result === true){
            res.redirect('/home');
        }else{
            res.send("Something went wrong on User Removal!")
        }
    })
});




app.get('/', isAuthenticated,
  function(req, res) {
      res.redirect('home');
  });

app.get('/home',isAuthenticated,function(req,res){
    var buses;
    var users;
    var connections;
    DB.sql_query("SELECT * FROM `Bus`",function (result){
        buses = result;
        DB.sql_query("SELECT User.name, User.idUser, Bus_has_User.Bus_idBus FROM User LEFT JOIN Bus_has_User ON User.idUser = Bus_has_User.User_idUser;",function (result){
            users = result;
            res.render('home', {user: req.user, buses: buses, users: users});
        });
    });
});

app.get('/logout',isAuthenticated,
  function(req, res){
    console.log("logout");
    req.logout();
    res.redirect('login');
  });


app.get('*',isAuthenticated, function(req, res){
    res.redirect('home');
});

//This must be the last route!!!
app.get('*', function(req, res){
    res.redirect('login');
});
//The get Above must be the last!!!



var options = {key: privateKey, cert: certificate};
var server = https.createServer(options,app);

//Client connect
const wss = new WebSocket.Server({server});
var clientSockets = [];

wss.on('connection', function connection (ws) {
    console.log("Client connected!");
    clientSockets.push(ws);
    ws.on('message',function message(msg){
        //console.log("Message from a client: "+msg);
    });

    ws.on('close', function close(){
        console.log("Client disconnected");
    });

    ws.on('error',function(error){
        if(error.toString() === "Error: connect ECONNREFUSED 127.0.0.1:3000"){
            console.log("Did not find Server.");
        }else{
            console.log('Unknown error occurred: '+error);
        }
    })
});

server.listen(8443, function listening () {
    console.log(""+server.address().port);

    var reconnectInterval = 5 * 1000;
    var ws;
    var connect = function(){
        ws = new WebSocket("wss://10.22.191.133:3000" , {//`wss://18.188.200.130:3000`, {
            rejectUnauthorized: false
        });


        ws.on('open', function open () {
            console.log("Connected to Websocket-server!");
            dbAuth.getLoginToken(DB,function (res){
                ws.send('imaWebserver:'+res);
            });
        });

        ws.on('message',function (msg){
            if(msg === "Update"){
                wss.emit("Update from Server!");
            }
            console.log("Message from NodeJs server :"+ msg);
        });

        ws.on('error',function(error){
            if(error.toString() === "Error: connect ECONNREFUSED 127.0.0.1:3000"){
                console.log("Did not find Server.");
            }else if(error.toString() === "Error: read ECONNRESET"){
                console.log("Server went down!")
            }else{
                console.log('Unknown error occurred: '+error);
            }
        });

        ws.on('close', function() {
            console.log('Websocket-server connection closed, trying to reconnect!');
            setTimeout(connect, reconnectInterval);
        });
    };
    connect();
    //Connection to the remote NodeJS server
});