# Sources

These are sources we have used or been influenced by:

* [1]“jquery - Live search through table rows,” Stack Overflow. [Online]. Available: https://stackoverflow.com/questions/12433304/live-search-through-table-rows?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa. [Accessed: 23-May-2018].
* [2]“Transparent Textures.” [Online]. Available: https://www.transparenttextures.com/. [Accessed: 23-May-2018].
* [3]“Demo for Passport.js authentication in a Node.js Express application,” Gist. [Online]. Available: https://gist.github.com/Xeoncross/bae6f2c5be40bf0c6993089d4de2175e. [Accessed: 08-Jun-2018].
