crypto = require('crypto');

function Auth(){

    var genRandomString = function(length){
        return crypto.randomBytes(Math.ceil(length/2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0,length);   /** return required number of characters */
    };

    /**
     * Private pbkdf2 function used to hash and salt and iterate the the secret.
     * @param secret    The secret, usually the password, that needs to be hashed with given salt.
     * @param salt      The salt.
     * @param callback  The callback function
     */
    var pbkdf2 = function(secret, salt, callback){
        crypto.pbkdf2(secret, salt,1000,64, 'sha512',function(err,buffer){
            if(err){
                callback(null)
            }else {
                console.log(buffer.toString('hex'));
                callback(buffer.toString('hex'));
            }

        });
    };

    this.GenerateSaltHashPassword = function(userpassword,callback) {
        var salt = genRandomString(16); /** Gives us salt of length 16 */
        pbkdf2(userpassword,salt,function(res){
            if(res){
                callback({passwordHash:res,salt:salt});
            }else{
                callback(null);
            }
        })
    };

    /**
     * Just a simple hash function made a public function.
     * @param secret  The string needing to be hashed.
     * @param callback The callback function.
     */
    this.hashPassword = function(secret,callback){
        pbkdf2(secret,"salt",function(res){
            callback(res);
        });
    };

    /**
     * This function is part of the authentication-process a mobile-client has to pass in order to access a websocket-connection.
     * In more detail it "hash and salt" then compares with the data gathered from the database and returns boolean value.
     * @param username      The Username.
     * @param pwd           The password.
     * @param salt          The salt from database.
     * @param usernameH     The hashed-password from database.
     * @param pwdH          The hashed-salt-password from database.
     * @param callback      Callback function
     * @returns {boolean}   The compare result.
     */
    this.mobileUserValidate = function(username, pwd, salt, usernameH, pwdH,callback){
        this.hashPassword(username,function(hashedUsername){
            if (hashedUsername === usernameH){
                pbkdf2(pwd,salt,function(hashedPwd){
                    if((hashedPwd === pwdH)){
                        //generate and return token.
                        callback(true)
                    }else{
                        console.log("Compared unsuccessfully!");
                        callback(null);
                    }
                });
            }else{
                callback(null);
            }
        });
    }

}



module.exports = Auth;