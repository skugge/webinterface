
var DB = require('./db.js');
var db = new DB();
const AUTH = require('./auth.js');
var auth = new AUTH();

var records = [];


exports.findById = function(id, callback) {
    process.nextTick(function() {
        function checkTable(){
            return new Promise(resolve => {
                for(var i = 0; i < records.length; i += 1){
                    if (records[i].id === id){
                        resolve(records[i]);
                    }
                    if(i===records.length && records[i].id !== id){
                        resolve(false)
                    }
                }
            })
        }

        async function f1(callback){
            var result = await checkTable();

            if (result === false){
                callback(new Error('User ' + id + ' does not exist'),null);
            }else{
                callback(null, result);
            }
        }
        f1(callback);
    })
};

exports.findByUsernameInDB = function(username, password, cb) {
    process.nextTick(function() {
        function getData(){
            return new Promise(resolve => {
                auth.hashPassword(username,function(usernameHash){
                    db.sql_query('SELECT * FROM `User` WHERE `username` LIKE "'+usernameHash+'"',function(res){
                        console.log(res);
                        if(res === []){
                            resolve(null);
                        }else{
                            resolve(res);
                        }
                    })
                });
            })
        };

        async function f1() {
            var result = await getData();
            if (result){
                console.log("Data sendt intro validate as parameters: \n"+username+" "+password+" "+result[0].salt+" "+result[0].username+" "+result[0].pwd);
                auth.mobileUserValidate(username,password,result[0].salt,result[0].username,result[0].pwd,function(res) {
                    if (res) {
                        var record = {
                            id: result[0].idUser,
                            username: result[0].username,
                            password: "",
                            displayName: result[0].name
                        };
                        records.push(record);
                        //console.log(records);
                        return cb(null, record);
                    } else {
                        return cb(null, null);
                    }
                })
            }else{
                return cb(null, null);
            }
        };

        f1();

    });
};


exports.authUserById = function(id,cb){
    function checkTable(){
        return new Promise(resolve => {
            for(var i = 0; i < records.length; i += 1){
                console.log(records);
                console.log(records[i]);
                if (records[i].id === id){
                    console.log("Hei, jeg fant jeg fant");
                    resolve(records[i]);
                }
            }
            resolve(false);
        })
    }

    async function f1(){
        var result = await checkTable();

        if (result === false){
            cb(new Error('User ' + id + ' does not exist'));
        }else{
            cb(null, result);
        }
    }
    f1();
};


exports.findByUsername = function(username) {
    process.nextTick(function() {
        return username in records;
    });
};