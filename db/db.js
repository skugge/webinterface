var fs = require("fs");
var mysql = require('mysql');
var async = require('async');
var path = require('path');


/**
 * The Database manager class (object)
 * This class is called on communication with the database.
 * It keeps the mysql connection-pool and handles all sql-query's.
 */
function Db(){
    var filePath = path.join(__dirname, '../certificates/dbCredentials.txt');
    var str = fs.readFileSync(filePath, {encoding: 'utf-8'}).toString();
    str = str.split(',');
    var pool;

    /**
     * The init function
     * Initiates the connection-pool.
     */
    this.init = function(){
        pool  = mysql.createPool({
            connectionLimit : 10,
            host            : 'mysql.stud.iie.ntnu.no',
            user            : str[0],
            password        : str[1],
            database        : str[0]
        });
    };

    /**
     * The Sql-query handler, it's job is to get a connection from the connection-pool, ask the database, and return the answer through a callback.
     *
     * @param sql       The Sql-query in string-format.
     * @param callback  The callback-function, makes the function caller wait for this function to respond with data.
     */
    this.sql_query = function(sql, callback){
        function dbconnect(sql){
            return new Promise(resolve => {
                pool.getConnection(function(err, connection) {
                    // Use the connection
                    connection.query(sql, function (error, results, fields) {
                        // And done with the connection.
                        connection.release();
                        // Handle error after the release.
                        if (error){
                            console.log(error);
                            resolve("false");
                        };
                        resolve(results)
                        // Don't use the connection here, it has been returned to the pool.
                    });
                });
            });
        }

        async function f1() {
            var result = await dbconnect(sql);
            callback(result)
        }
        f1();
    };
    this.init();
}



module.exports = Db;