


function  DbQuerys(Db,Auth){
    
    this.db = Db;
    this.auth = Auth;
    
    this.addNewUser = function (name, username, pwd, admin,callback) {
        var thisis = this;
        if(name.length <= 50 && username.length <= 128 && pwd.length <= 128 && admin.length === 1){
            thisis.auth.GenerateSaltHashPassword(pwd,function(pwdData){
                if(pwdData){
                    console.log("pwd : "+pwdData.passwordHash+" salt: "+pwdData.salt);
                    thisis.auth.hashPassword(username,function(usernameHash){
                        if(usernameHash){
                            thisis.db.sql_query("INSERT INTO `User` (`idUser`, `name`, `username`, `pwd`, `salt`, `Bus_idBus`, `active`, `token`, `admin`) VALUES (NULL, '"+name+"', '"+usernameHash+"', '"+pwdData.passwordHash+"', '"+pwdData.salt+"', NULL, '0', NULL, '"+admin+"');",function (res) {
                                console.log(res);
                                if (res.affectedRows === 1) {
                                    callback(true);
                                }else{
                                    callback(false);
                                }
                            })
                        }
                    });
                }
            });
        }else{
            console.log("Wrong in input");
            callback(false);
        }
    };

    this.removeUser = function(id,callback){
        this.db.sql_query("DELETE FROM `User` WHERE `User`.`idUser` = '"+id+"'",function(res){
            if(res.affectedRows === 1){
                callback(true)
            }else{
                callback(false)
            }
        })
    };
    
}


module.exports  = DbQuerys;