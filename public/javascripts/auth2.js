

$(document).ready(function(){
    $("#local-sign-in").submit(function(e){
        e.preventDefault();
        var pswElement =document.getElementById("psw");
        var hash = CryptoJS.SHA256(pswElement.value.toString());
        var psw = $('#psw');
        psw.val(hash.toString(CryptoJS.enc.Hex));
        console.log(pswElement.value.toString());
        this.submit();
    });

    $("#newUser").submit(function(e){
        e.preventDefault();
        var pswElement = document.getElementById("psw");
        var hash = CryptoJS.SHA256(pswElement.value.toString());
        var psw = $('#psw');
        psw.val(hash.toString(CryptoJS.enc.Hex));
        console.log(pswElement.value.toString());
        this.submit();
    });
});