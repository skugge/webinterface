/**
 * Created by rebekkaheggebo on 19.04.2018.
 */
$(document).ready(function() {
    $('.container-fluid').on('click', function(e)  {
        $("li.active").removeClass("active");
        var currentAttrValue = $(this).attr('href');

        // Show/Hide Tabs
        $('.tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active);
        $(this).parent('li').addClass('active').siblings().removeClass('active');
        e.preventDefault();
    });
});

