/**
 * Created by rebekkaheggebo on 26.04.2018.
 */
//Based on the example by user "Nope" on stack exchange
// https://stackoverflow.com/questions/12433304/live-search-through-table-rows?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

//Function searches through table of busses
$(document).ready(function() {

    $("#search").on("keyup", function () {
        var searchInput = $(this).val();
        $("table tr").each(function (index) {
            if (index !== 0) {
                $row = $(this);
                var id = $row.find("td:first").text();
                if (id.indexOf(searchInput) !== 0) {
                    $row.hide();
                }
                else {
                    $row.show();
                }
            }
        });
    });
});