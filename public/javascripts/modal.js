/**
 * Created by rebekkaheggebo on 17.04.2018.
 */

//function sends user to top of page
function toTheTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}


$(document).ready(function(){

    //opens the edit playback modal
    $(".modalOpenButton").on("click", function(e){
        var source = $(this);
        var bus = $("#" + source.attr("id")).find(".busField").text();
        var channel = $("#" + source.attr("id")).find(".channelField").text();
        var active = $("#" + source.attr("id")).find(".activeField").text();
        $("#myModal").find("#modalBusField").val(" " + bus);
        $("#myModal").find("#modalChannelField").val(" " + channel);
        $("#myModal").find("#modalActiveField").val(" " + active);
        $("#myModal").modal("show");
    })

    //opens the delete user modal
    $(".modalUserButton").on("click", function(e){
        var source = $(this);
        var user = $("#" + source.attr("id")).find(".userField").text();
        var id = $("#" + source.attr("id")).find(".idField").text();
        $("#userDeleteModal").find("#modalNameField").val(user);
        $("#userDeleteModal").find("#modalIdField").val(id);
        $("#userDeleteModal").modal("show");
    })
})
